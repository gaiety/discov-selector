import { expect, test } from 'bun:test'
import discov from '../index.ts'

test('can select a simple passed in element', () => {
  expect(
    discov(document.body)
    .element
  ).toBe(document.body)
})

test('can target by tag', () => {
  document.body.innerHTML = `<button>My button</button>`

  let buttonElement = document.querySelector('button')
  expect(
    discov(document.body)
    .tag('button')
    .element
  ).toBe(buttonElement)
})

test('can target by class', () => {
  document.body.innerHTML = `<button class='my-class'>My button</button>`

  let buttonElement = document.querySelector('button')
  expect(
    discov(document.body)
    .class('my-class')
    .element
  ).toBe(buttonElement)
})

test('can target by tag with class', () => {
  document.body.innerHTML = `<button class='my-class'>My button</button>`

  let buttonElement = document.querySelector('button')
  expect(
    discov(document.body)
    .tag('button')
    .class('my-class')
    .element
  ).toBe(buttonElement)
})
