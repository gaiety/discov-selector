class Discov {
  readonly rootElement: HTMLElement = document
  baseElement: HTMLElement
  query: string = ''

  constructor (rootElement?: HTMLElement) {
    if (rootElement !== undefined) this.rootElement = rootElement
    this.baseElement = this.rootElement
    this.query = ''
  }

  tag(tagName: string): HTMLElement {
    this.query += ` ${tagName}`
    return this
  }

  class(className: string): HTMLElement {
    if (className.charAt(0) == '.') console.warn('class target contains a "." and may not match')
    this.query += `.${className}`
    return this
  }

  get element(): HTMLElement {
    if (this.query == '') return this.baseElement
    return this.baseElement.querySelector(this.query)
  }
}

export default (rootElement?: HTMLElement) => new Discov(rootElement)
